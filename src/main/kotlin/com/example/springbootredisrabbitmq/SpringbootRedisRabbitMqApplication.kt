package com.example.springbootredisrabbitmq

import com.example.springbootredisrabbitmq.config.RabbitConfig
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import

@EnableAutoConfiguration
@ComponentScan
@Import(RabbitConfig::class)
@EnableRabbit
class SpringbootRedisRabbitMqApplication

fun main(args: Array<String>) {
    runApplication<SpringbootRedisRabbitMqApplication>(*args)
}
