package com.example.springbootredisrabbitmq.service

import com.example.springbootredisrabbitmq.config.CacheManagerConfig
import com.example.springbootredisrabbitmq.config.HashConfig
import com.example.springbootredisrabbitmq.model.Message
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service

@Service
@EnableRabbit
class QueueProducerService(private val cacheManager: CacheManagerConfig) {

    private val logger = LoggerFactory.getLogger(QueueProducerService::class.java)

    @RabbitListener(queues = ["myQueue"])
    fun receiveMessage(msg: Message) {
        val hashing:String = msg.topic + msg.subject
        val result:String = HashConfig.hashString(hashing, "SHA-256")

        if (!cacheManager.isAlreadySend(result)) {
            // calling the method that sends the message
            cacheManager.markAsSent(result)
            logger.info("received msg = {}", msg)
        } else {
            logger.info("message was already sent")
        }
    }
}
//@RabbitListener(queues = [ServiceQueues.OUTGOING_BATCH_EMAIL])
//fun receiveFromBatchQueue(message: EmailMessage) {
//
//    val hashMessage: String =
//        generateHash(message.receiver, message.subject, message.message)
//
//    val cacheKey = "sent-flag:$hashMessage"
//
//    if (!multipleSendGuard.isMessageSent(cacheKey)) {
//        handleMessage(message)
//        multipleSendGuard.markMessageAsSent(cacheKey)
//    } else {
//        log.info("Message was already sent to receiver={}", message.receiver)
//    }
//}