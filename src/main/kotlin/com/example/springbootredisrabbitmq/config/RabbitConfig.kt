package com.example.springbootredisrabbitmq.config


import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.amqp.core.Queue

@Configuration
class RabbitConfig {
    @Bean
    fun myQueue(): Queue = Queue("myQueue")
}