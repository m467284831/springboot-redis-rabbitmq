package com.example.springbootredisrabbitmq.config

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

object HashConfig {
    fun hashString(input: String, algorithm: String):String {
        return try {
            val md = MessageDigest.getInstance(algorithm)
            val hashedBytes = md.digest(input.toByteArray())

            val sb = StringBuilder()
            for (b in hashedBytes) {
                sb.append(String.format(Locale.US,"%02x", b))
            }
            sb.toString();

        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        }
    }
}
