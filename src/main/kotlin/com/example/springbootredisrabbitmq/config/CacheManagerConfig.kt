package com.example.springbootredisrabbitmq.config

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
class CacheManagerConfig {
    companion object {
        const val ALREADY_SENT: Boolean = false
        const val MARKED_AS_SENT: Boolean = true
    }

    @CachePut(key = "#key", value = ["myCache"])
    fun markAsSent(key: String) = MARKED_AS_SENT

    @Cacheable(key = "#key", value = ["myCache"], unless = "#result == false")
    fun isAlreadySend(key: String) = ALREADY_SENT
}