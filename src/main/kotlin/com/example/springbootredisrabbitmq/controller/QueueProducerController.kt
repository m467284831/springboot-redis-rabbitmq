package com.example.springbootredisrabbitmq.controller

import com.example.springbootredisrabbitmq.model.Message
import com.example.springbootredisrabbitmq.service.QueueProducerService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.http.ResponseEntity

@RestController
@RequestMapping("/queue")
class QueueProducerController(private val service: QueueProducerService) {

    @PostMapping("/emit")
    fun sendMessage(@RequestBody msg: Message): ResponseEntity<String> {
        service.receiveMessage(msg)
        return ResponseEntity.ok("Success emit to queue")
    }
}
