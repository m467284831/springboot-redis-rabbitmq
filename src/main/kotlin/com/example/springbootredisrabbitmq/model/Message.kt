package com.example.springbootredisrabbitmq.model

class Message {
    var topic: String? = null
    var subject: String? = null
}